# WIP: `agepoly.ch` new site backend

## Development

### Installation

Make sure to install the following system dependencies:
- [Pipenv](https://docs.pipenv.org/) with a working `python3>=3.6` installation
- `python3-pip`, `python3-virtualenv`, `pipenv` `tox`

To start hacking simply make sure to install development dependencies:
```shell
pipenv install --dev
```

If you're running nixos, the following line may be useful:
`nix-shell -p 'python3.withPackages(ps: with ps; [ipython tox virtualenv pip pipenv])'`.
After running `pipenv install` (see above), activating the virtualenv suffices.

### Running

- Execute tests by running `tox`
- Start the application using `python agepoly_site_backend`
