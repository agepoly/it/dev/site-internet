import unittest

from api import app


class TestAPI(unittest.TestCase):
    def setUp(self):
        app.testing = True
        self.app = app.test_client()

    def test_ping(self):
        res = self.app.get('/_status/healthcheck')
        assert b'OK' in res.data
