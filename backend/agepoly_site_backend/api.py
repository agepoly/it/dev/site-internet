"""Top level Flask Application
This class is the top-level flask application providing an HTTP REST web API.
"""
from datetime import datetime
import logging

from flask import Flask, request
from werkzeug.exceptions import HTTPException


# ################################################## Prelude and util functions

app = Flask(__name__)
log = logging.getLogger(__name__)


@app.route('/_status/healthcheck')
def ping():
    """Return a friendly HTTP pong."""
    return 'OK'


# #############################################################  Error Handerls


@app.errorhandler(KeyError)
def handle_invalid_resource(exception):
    log.exception('Invalid request %s: %s', request.url, str(exception))
    status_code = getattr(exception, 'status_code', 400)
    return str(exception), status_code


@app.errorhandler(ValueError)
def handle_invalid_usage(exception):
    log.exception('Invalid request %s: %s', request.url, str(exception))
    status_code = getattr(exception, 'status_code', 400)
    return str(exception), status_code


@app.errorhandler(Exception)
def handle_server_errors(exception):
    if isinstance(exception, HTTPException):
        return exception
    now = str(datetime.now())
    reference = hash('{}-{}'.format(now, str(exception)))
    log.warning('Request caused server error (%s). url: %s data: %s',
                reference, str(request.url), str(request.data))
    log.exception('Server-side error with reference %s', reference)
    return ('A server-side error happened. If you report the issue, '
            'please mention the following reference number: '
            '{}.'.format(reference)), 500
